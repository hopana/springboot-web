package com.hp.springboot.dao;

import com.hp.springboot.pojo.model.UserOrder;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 用户DAO
 *
 * @author hupan
 * @since 2018-05-09 11:30
 */
@Mapper
public interface UserOrderDao {

    @Select("select * from user_order where id = #{id}")
    UserOrder getById(@Param("id") Long id);

    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    @Insert("insert into user_order(order_no, user_id, pay_amount, total_amount, delivery_addr, status, create_time, update_time) " +
            "values(#{orderNo} , #{userId}, #{payAmount} , #{totalAmount} , #{deliveryAddr} , #{status} , #{createTime}, #{updateTime})")
    int saveUserOrder(UserOrder userOrder);

}
