package com.hp.springboot.dao;

import com.hp.springboot.pojo.model.Area;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 角色DAO
 *
 * @author hupan
 * @since 2018-05-09 11:30
 */
@Mapper
public interface AreaDao {

    @Select("select * from ti_area where area_id = #{areaId}")
    Area getById(@Param("areaId") int areaId);

    @Select("select * from ti_area where parent_area_id=0")
    Area getRoot();
}
