package com.hp.springboot.dao;

import com.hp.springboot.pojo.model.TGood;
import org.apache.ibatis.annotations.Mapper;


/**
 *
 * @author hupan
 * @date 2022-01-13 22:56
 */

@Mapper
public interface TGoodDao {

    int deleteByPrimaryKey(Integer id);

    int insert(TGood record);

    int insertSelective(TGood record);

    TGood selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TGood record);

    int updateByPrimaryKey(TGood record);
}