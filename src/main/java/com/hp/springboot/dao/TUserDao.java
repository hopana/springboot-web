package com.hp.springboot.dao;

import com.hp.springboot.pojo.model.TUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * 测试
 *
 * @author hupan
 * @date 2022-01-13 15:25
 */
@Mapper
public interface TUserDao {

    int deleteByPrimaryKey(Long id);

    int insert(TUser record);

    int insertSelective(TUser record);

    TUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TUser record);

    int updateByPrimaryKey(TUser record);

}