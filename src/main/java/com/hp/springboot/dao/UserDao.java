package com.hp.springboot.dao;

import com.hp.springboot.pojo.model.User;
import org.apache.ibatis.annotations.*;

/**
 * 用户DAO
 *
 * @author hupan
 * @since 2018-05-09 11:30
 */
@Mapper
public interface UserDao {

    @Select("select * from user where id = #{id}")
    User getById(@Param("id") Long id);

    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    @Insert("insert into user(name, age, id_card, gender, mobile, address, create_time, update_time) values(#{name}, #{age}, #{idCard}, #{gender}, #{mobile}, #{address}, #{createTime}, #{updateTime})")
    int saveUser(User user);

    @Select("select * from user where mobile = #{mobile}")
    User getByMobile(@Param("mobile") String mobile);
}
