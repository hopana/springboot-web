package com.hp.springboot.controller;

import com.hp.springboot.pojo.dto.Person;
import com.hp.springboot.result.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 测试参数绑定
 *
 * @author hupan
 * @since 2018-04-09 15:52
 */
@Controller
@RequestMapping("/test/param")
public class ParamController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ParamController.class);

    @ResponseBody
    @RequestMapping("/hello")
    public Result<Object> test(Person p, Integer start, Integer pageSize) {
        LOGGER.info("------------------测试参数------------------");
        LOGGER.info("对象参数：{}", p);
        LOGGER.info("分页参数：{}-{}", start, pageSize);

        return Result.success();
    }

}
