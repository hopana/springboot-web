package com.hp.springboot.controller;

import com.hp.springboot.pojo.groups.A;
import com.hp.springboot.pojo.model.Employee;
import com.hp.springboot.pojo.model.Person;
import com.hp.springboot.pojo.model.SfWorker;
import com.hp.springboot.pojo.model.Worker;
import com.hp.springboot.result.Result;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * index请求处理
 *
 * @author hupan
 * @since 2018-03-28 15:49
 */
@Validated
@Controller
@RequestMapping("/valid")
public class ValidController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidController.class);

    @PostMapping("/test1")
    @ResponseBody
    public Result<Employee> test1(@Valid Employee employee) {
        return Result.success(employee);
    }

    @PostMapping("/test2")
    @ResponseBody
    public Result<Worker> test2(@Validated(A.class) Worker worker) {
        return Result.success(worker);
    }

    /**
     * 测试Model有继承时，父类里面的字段校验是否生效
     * 结论：父类属性可以正常校验
     */
    @PostMapping("/test3")
    @ResponseBody
    public Result<Person> test3(@Validated SfWorker worker) {
        return Result.success(worker);
    }

}
