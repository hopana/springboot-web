package com.hp.springboot.result;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 全局返回结果封装
 *
 * @author hupan
 * @date 2021/12/17
 */
@Getter
@Setter
@Accessors(chain = true)
@ToString
public class Result<T> {

    private String code;
    private String msg;
    private String detail;
    private T data;

    /**
     * 成功时候的调用
     */
    public static <T> Result<T> success() {
        return new Result<>(CodeMsg.SUCCESS);
    }

    /**
     * 成功时候的调用
     */
    public static <T> Result<T> success(T data) {
        Result<T> result = new Result<>();
        result.setCode(CodeMsg.SUCCESS.getCode());
        result.setMsg(CodeMsg.SUCCESS.getMsg());
        result.setData(data);
        return result;
    }

    /**
     * 失败时候的调用
     */
    public static <T> Result<T> error(CodeMsg codeMsg) {
        return new Result<>(codeMsg);
    }

    /**
     * 失败时候的调用
     */
    public static <T> Result<T> error(String code, String msg) {
        return new Result<>(code, msg);
    }

    public Result<T> detail(String detail) {
        this.detail = detail;
        return this;
    }

    /**
     * 失败时候的调用
     */
    public static <T> Result<T> error(String code, String msg, String detail) {
        return new Result<>(code, msg, detail);
    }

    private Result(T data) {
        this.data = data;
    }

    public Result() {

    }

    public Result(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Result(String code, String msg, String detail) {
        this.code = code;
        this.msg = msg;
        this.detail = detail;
    }

    public Result(CodeMsg codeMsg) {
        if (codeMsg != null) {
            this.code = codeMsg.getCode();
            this.msg = codeMsg.getMsg();
        }
    }

}
