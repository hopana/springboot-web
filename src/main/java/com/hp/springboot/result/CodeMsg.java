package com.hp.springboot.result;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum CodeMsg {

    /**
     * 通用的错误码
     */
    SUCCESS("200", "success"),
    PARAM_ERROR("400", "参数错误"),
    UNAUTHORIZED("401", "未授权"),
    NOT_SUPPORT("402", "不支持该请求"),
    NOT_FOUND("404", "请求的资源不存在"),

    /**
     * 服务端错误码
     */
    SERVER_ERROR("500100", "服务器繁忙"),
    SIGN_ERROR("500101", "签名错误"),
    EMPTY_TOKEN("500102", "TOKEN为空"),
    AUTH_FAIL("500103", "认证失败"),
    INVALID_PARAMETERS("500104", "参数不合法"),
    BIND_ERROR("500105", "参数校验异常"),
    REQUEST_ILLEGAL("500106", "请求非法"),
    ACCESS_LIMIT_REACHED("500107", "访问太频繁！"),
    UPLOAD_FAILED("500108", "文件上传失败！"),
    RPC_FAILED("500109", "RPC接口调用失败！"),
    OPERATION_LIMITED("500110", "操作被限制！"),
    BE_BLOCKED("500111", "内容不合规，更改资料失败"),
    NEED_REVIEW("500112", "提交成功，稍后将为你审核"),
    NOT_EXIST("500113", "记录不存在"),
    NOT_CONSISTENT("500114", "记录不一致"),
    NOT_MATCH("500115", "数据不匹配"),
    FOUND_EXISTED("500116", "请求的资源已经存在"),
    OPERATOR_TOO_MUCH("500117", "操作太频繁，请稍后重试");
    

    private final String code;
    private final String msg;
    private String detail;

    CodeMsg(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public CodeMsg detail(String detail) {
        this.detail = detail;
        return this;
    }

}
