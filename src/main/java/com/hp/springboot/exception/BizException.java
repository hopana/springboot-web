package com.hp.springboot.exception;

import com.hp.springboot.exception.fast.FastCheckedException;
import com.hp.springboot.result.CodeMsg;

/**
 * 业务异常
 *
 * @author hupan
 * @date 2021-12-16
 */
public class BizException extends FastCheckedException {

    private CodeMsg codeMsg;

    public BizException(CodeMsg codeMsg) {
        super(codeMsg.getMsg());
        this.codeMsg = codeMsg;
    }

    public CodeMsg getCodeMsg() {
        return codeMsg;
    }

    public BizException(String msg) {
        super(msg);
    }

    public BizException(String code, String msg) {
        super(code, msg);
    }

    public BizException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public BizException(String code, String msg, Throwable cause) {
        super(code, msg, cause);
    }

    public BizException(CodeMsg codeMsg, Throwable cause) {
        super(String.valueOf(codeMsg.getCode()), codeMsg.getMsg(), cause);
    }

}
