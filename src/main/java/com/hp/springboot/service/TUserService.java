package com.hp.springboot.service;

import com.hp.springboot.dao.TUserDao;
import com.hp.springboot.pojo.model.TUser;
import com.hp.springboot.result.Result;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 用户
 *
 * @author hupan
 * @since 2018-05-09 14:53
 */
@Slf4j
@Service
public class TUserService {
    @Resource
    private TUserDao tUserDao;


    public Result<Integer> save(TUser user) {
        int rows = tUserDao.insertSelective(user);
        return Result.success(rows);
    }

    public Result<Integer> update(TUser user) {
        int rows = tUserDao.updateByPrimaryKeySelective(user);
        return Result.success(rows);
    }

    public Result<Integer> delete(TUser user) {
        int rows = tUserDao.deleteByPrimaryKey(user.getId());
        return Result.success(rows);
    }


}
