package com.hp.springboot.service;

import com.hp.springboot.dao.TGoodDao;
import com.hp.springboot.pojo.model.TGood;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;


/**
 * @author hupan
 * @date 2022-01-13 22:56
 */

@Service
public class TGoodService {

    @Resource
    private TGoodDao tGoodDao;


    public int deleteByPrimaryKey(Integer id) {
        return tGoodDao.deleteByPrimaryKey(id);
    }


    public int insert(TGood record) {
        return tGoodDao.insert(record);
    }


    public int insertSelective(TGood record) {
        return tGoodDao.insertSelective(record);
    }


    public TGood selectByPrimaryKey(Integer id) {
        return tGoodDao.selectByPrimaryKey(id);
    }


    public int updateByPrimaryKeySelective(TGood record) {
        return tGoodDao.updateByPrimaryKeySelective(record);
    }


    public int updateByPrimaryKey(TGood record) {
        return tGoodDao.updateByPrimaryKey(record);
    }

}

