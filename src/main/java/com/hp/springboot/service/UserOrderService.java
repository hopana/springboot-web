package com.hp.springboot.service;

import com.hp.springboot.dao.UserOrderDao;
import com.hp.springboot.pojo.model.UserOrder;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 用户
 *
 * @author hupan
 * @since 2018-05-09 14:53
 */
@Slf4j
@Service
public class UserOrderService {

    @Resource
    private UserOrderDao userOrderDao;

    public void saveUser(UserOrder userOrder) {
        userOrderDao.saveUserOrder(userOrder);
    }

    public UserOrder queryById(Long id) {
        return userOrderDao.getById(id);
    }

    public UserOrder testGetOrderById(Long id) {
        UserOrder order = userOrderDao.getById(id);
        log.info(order.toString());
        return order;
    }

}
