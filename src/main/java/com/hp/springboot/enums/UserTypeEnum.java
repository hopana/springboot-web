package com.hp.springboot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author hupan
 * @date 2021-09-24
 */
@Getter
@AllArgsConstructor
public enum UserTypeEnum {

	Java("000", "Java开发工程师"),
	DB("001", "数据库管理员"),
	LINUX("002", "Linux运维员");

	private String value;
	private String title;

}
