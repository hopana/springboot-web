package com.hp.springboot.pojo.groups;

import javax.validation.GroupSequence;

/**
 * 默认的组验证顺序
 *
 * @author hupan
 * @date 2021-12-17
 */
@GroupSequence( { Default.class, A.class, B.class, C.class, D.class})
public interface Group {

}
