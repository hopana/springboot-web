package com.hp.springboot.pojo.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto1 {

    private Long id;
    private String name;
    private int age;
    /**
     * 实体类该属性是Date
     */
    private String createTime;
    /**
     * 实体类该属性是String
     */
    private Date updateTime;

}
