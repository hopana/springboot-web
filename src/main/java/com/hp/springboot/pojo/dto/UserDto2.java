package com.hp.springboot.pojo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author hupan
 * @date 2021-09-24
 */
@Getter
@Setter
@ToString
public class UserDto2 {

	private Integer id;
	private String name;
	private String type;

}
