package com.hp.springboot.pojo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @author hupan
 * @date 2021-10-08
 */
@Getter
@Setter
@ToString
public class DeliveryAddressDto {

	private String address;
	private Integer houseNumber;
	private String description;
	private Date  deliveryTime;
	private Integer delFlag;

}
