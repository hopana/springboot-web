package com.hp.springboot.pojo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author hupan
 * @date 2021-09-24
 */
@Getter
@Setter
@ToString
public class AnimalDto {

	private String name;
	private int age;
	private String desc;
	private UserDto2 owner;
}
