package com.hp.springboot.pojo.dto;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author hupan
 * @date 2021-10-08
 */
@Getter
@Setter
@ToString
public class Person {

	private String name;
	private Integer age;
	private String description;
	private Date orderTime;

}
