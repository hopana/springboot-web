package com.hp.springboot.pojo.dto;

import lombok.*;

import java.util.Date;

/**
 * @author hupan
 * @date 2021-09-22
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CarDto {

	private String make;
	private int seatCount;
	private String type;
	private String manufacturer;

	private String owner;

	private Date modifyTime;

}