package com.hp.springboot.pojo.mapper;

import com.hp.springboot.pojo.dto.DeliveryAddressDto;
import com.hp.springboot.pojo.dto.Person;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.Date;

/**
 * @author hupan
 * @date 2021-10-08
 */
@Mapper(imports = Date.class)
public interface AddressMapper {

	AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

	@Mapping(source = "person.description", target = "description")
	@Mapping(source = "hn", target = "houseNumber")
	DeliveryAddressDto personAndAddressToDeliveryAddressDto(Person person, Integer hn);

	@Mapping(source = "person.description", target = "description")
	@Mapping(target = "deliveryTime", expression = "java(new Date())")
	DeliveryAddressDto personToDeliveryAddressDto(Person person);

	@Mapping(source = "person.description", target = "description")
	@Mapping(target = "deliveryTime", expression = "java(new Date())")
	@Mapping(target = "delFlag", constant = "0")
	DeliveryAddressDto personToDeliveryAddressDtoUseConstant(Person person);

	@Mapping(source = "person.description", target = "description")
	@Mapping(target = "deliveryTime", expression = "java(new Date())")
	@Mapping(target = "delFlag", constant = "1")
	DeliveryAddressDto personToDeliveryAddress(Person person);

}