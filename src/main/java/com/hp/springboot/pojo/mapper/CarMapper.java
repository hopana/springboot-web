package com.hp.springboot.pojo.mapper;


import com.hp.springboot.pojo.dto.CarDto;
import com.hp.springboot.pojo.model.Car;
import com.hp.springboot.pojo.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.Date;

/**
 * 转换器
 *
 * @author hupan
 * @date 2021-09-22
 */
@Mapper(imports = Date.class)
public interface CarMapper {

	CarMapper INSTANCE = Mappers.getMapper(CarMapper.class);

	@Mapping(source = "make", target = "manufacturer")
	@Mapping(source = "numberOfSeats", target = "seatCount")
	CarDto carToCarDto(Car car);

	@Mapping(source = "car.make", target = "manufacturer")
	@Mapping(source = "car.numberOfSeats", target = "seatCount")
	@Mapping(source = "user.name", target = "owner")
	CarDto carToCarDto(Car car, User user);

	@Mapping(source = "car.make", target = "manufacturer")
	@Mapping(source = "car.numberOfSeats", target = "seatCount")
	@Mapping(source = "user.name", target = "owner")
	void updateTarget(Car car, User user, @MappingTarget CarDto carDto);

	@Mapping(source = "car.make", target = "manufacturer")
	@Mapping(source = "car.numberOfSeats", target = "seatCount")
	@Mapping(source = "user.name", target = "owner")
	CarDto updateTargetAndReturn(Car car, User user, @MappingTarget CarDto carDto);

	/**
	 * 注意source和constant不能同时存在
	 */
	@Mapping(target = "seatCount", constant = "5")
	@Mapping(source = "user.name", target = "owner", defaultValue = "hupan")
	void carToCarDto(Car car, User user, @MappingTarget CarDto carDto);

	/**
	 * 注意source和constant不能同时存在
	 */
	@Mapping(target = "seatCount", constant = "5")
	@Mapping(source = "updateTime", target = "modifyTime", defaultExpression = "java(new Date())")
	CarDto carToCarDtoWithExpression(Car car);

}