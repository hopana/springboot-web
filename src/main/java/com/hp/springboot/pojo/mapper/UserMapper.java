package com.hp.springboot.pojo.mapper;


import cn.hutool.core.date.DateUtil;
import com.hp.springboot.pojo.dto.UserDto1;
import com.hp.springboot.pojo.dto.UserDto2;
import com.hp.springboot.pojo.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * 转换器
 *
 * @author hupan
 * @date 2021-09-22
 */
@Mapper(imports = DateUtil.class)
public interface UserMapper {

	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

	@Mapping(target = "createTime", expression = "java(DateUtil.formatDateTime(user.getCreateTime()))")
	UserDto1 toUserDto1(User user);

	@Mapping(source = "userType", target = "type")
	UserDto2 toUserDto2(User user);

	@Mapping(target = "type", expression = "java(user.getUserType().getTitle())")
	UserDto2 toUserDto3(User user);

}