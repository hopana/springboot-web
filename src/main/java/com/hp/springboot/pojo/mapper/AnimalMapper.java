package com.hp.springboot.pojo.mapper;


import com.hp.springboot.pojo.dto.AnimalDto;
import com.hp.springboot.pojo.dto.UserDto2;
import com.hp.springboot.pojo.model.Animal;
import com.hp.springboot.pojo.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 转换器
 *
 * @author hupan
 * @date 2021-09-22
 */
@Mapper
public abstract class AnimalMapper {

	public static final AnimalMapper INSTANCE = Mappers.getMapper(AnimalMapper.class);

	public abstract AnimalDto animalToAnimalDto(Animal animal);

	public UserDto2 userToUserDto(User user) {
		UserDto2 userDto2 = new UserDto2();
		userDto2.setName(user.getName());
		userDto2.setType(user.getUserType().getTitle());

		return userDto2;
	}
}