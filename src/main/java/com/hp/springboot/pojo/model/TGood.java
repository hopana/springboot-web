package com.hp.springboot.pojo.model;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * TODO 注释
 *
 * @author hupan
 * @date 2022-01-13 22:56
 */

@Getter
@Setter
@ToString
public class TGood implements Serializable {

    private Integer id;

    private String goodCode;

    private String goodName;

    private static final long serialVersionUID = 1L;
}