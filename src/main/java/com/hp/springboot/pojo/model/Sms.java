package com.hp.springboot.pojo.model;

import lombok.*;
import lombok.experimental.Accessors;

import java.util.Date;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Sms {
    private Long id;
    private String mobile;
    private String content;
    private Date createTime;
    private Date updateTime;
}
