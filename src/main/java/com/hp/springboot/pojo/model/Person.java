package com.hp.springboot.pojo.model;

import javax.validation.constraints.NotBlank;

/**
 * @author hupan
 * @date 2021-12-24
 */
public class Person {

    @NotBlank(message = "身份证号不能为空")
    private String idCardNo;

}
