package com.hp.springboot.pojo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubDepot {

    private Integer subDepotId;
    private Integer pid;
    private String subDepotCode;
    private String subDepotName;
    private Integer depotId;

}
