package com.hp.springboot.pojo.model;

import com.hp.springboot.pojo.dto.UserDto1;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author hupan
 * @date 2021-09-24
 */
@Getter
@Setter
@ToString
public class Animal {

	private String name;
	private int age;
	private String desc;
	private User owner;
}
