package com.hp.springboot.pojo.model;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserOrder {

    private Long id;
    private String orderNo;
    private Long userId;
    private BigDecimal payAmount;
    private BigDecimal totalAmount;
    private String deliveryAddr;
    private Byte status;
    private Date createTime;
    private Date updateTime;

}
