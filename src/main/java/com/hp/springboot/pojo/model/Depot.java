package com.hp.springboot.pojo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Depot {

    private Integer depotId;
    private Integer pid;
    private String depotCode;
    private String depotName;
    private Integer areaId;

}
