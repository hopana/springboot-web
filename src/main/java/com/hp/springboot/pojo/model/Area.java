package com.hp.springboot.pojo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Area {

    private Integer areaId;
    private Integer parentAreaId;
    private String areaCode;
    private String areaName;

}
