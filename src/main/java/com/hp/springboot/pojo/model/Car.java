package com.hp.springboot.pojo.model;

import com.hp.springboot.enums.CarType;
import lombok.*;

import java.util.Date;

/**
 * @author hupan
 * @date 2021-09-22
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class Car {

	@NonNull
	private String make;
	@NonNull
	private int numberOfSeats;
	@NonNull
	private CarType type;

	private Date updateTime;

}