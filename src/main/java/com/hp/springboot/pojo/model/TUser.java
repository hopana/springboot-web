package com.hp.springboot.pojo.model;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



/**
 * @author hupan
 * @date 2022-01-13 15:25
 */

@Getter
@Setter
@ToString
public class TUser implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private String password;

    private String phone;

    private String address;

}