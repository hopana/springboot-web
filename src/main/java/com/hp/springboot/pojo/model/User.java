package com.hp.springboot.pojo.model;

import com.hp.springboot.enums.UserTypeEnum;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private Long id;
    private String name;
    private int age;
    private String idCard;
    private Integer gender;
    private String mobile;
    private String address;
    private Date createTime;
    private Date updateTime;

    private UserTypeEnum userType;

}
