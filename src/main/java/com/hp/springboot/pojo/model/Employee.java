package com.hp.springboot.pojo.model;

import java.util.Date;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 程序员对象
 *
 * @author hupan
 * @date 2021-12-16
 */
@Getter
@Setter
@ToString
public class Employee {

    @NotNull
    @Min(value = 1, message = "员工工号最小为1")
    @Max(value = 10000, message = "员工工号最大为10000")
    private Long no;
    @NotNull
    @NotBlank(message = "员工姓名不能为空")
    private String name;
    @NotNull
    @Min(value = 1, message = "员工年龄不能小于1岁")
    @Max(value = 120, message = "员工年龄不能超过120岁")
    private Integer age;
    @NotBlank(message = "员工职位不能为空")
    private String job;
    private String adress;
    private Date hireDate;
    @NotNull
    @Min(value = 1000, message = "员工薪资最少为1000美金")
    private Long salary;
    private Double bonusRate;

}
