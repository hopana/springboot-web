package com.hp.springboot.pojo.model;

import com.hp.springboot.pojo.groups.A;
import com.hp.springboot.pojo.groups.B;
import com.hp.springboot.pojo.groups.C;
import com.hp.springboot.pojo.groups.D;
import java.util.Date;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 程序员对象
 *
 * @author hupan
 * @date 2021-12-16
 */
@Getter
@Setter
@ToString
public class Worker {

    @NotNull(message = "员工工号不能为空", groups = {A.class, B.class, C.class})
    @Min(value = 1, message = "员工工号最小为1", groups = {A.class, B.class, C.class})
    @Max(value = 10000, message = "员工工号最大为10000", groups = {A.class, B.class, C.class})
    private Long no;
    @NotNull(message = "员工姓名不能为空", groups = {A.class, B.class})
    @NotBlank(message = "员工姓名不能为空",  groups = {A.class, B.class})
    private String name;
    @NotNull(message = "员工年龄不能为空", groups = {B.class, C.class})
    @Min(value = 1, message = "员工年龄不能小于1岁", groups = {B.class, C.class})
    @Max(value = 120, message = "员工年龄不能超过120岁", groups = {B.class, C.class})
    private Integer age;
    @NotBlank(message = "员工职位不能为空", groups = C.class)
    private String job;
    private String adress;
    private Date hireDate;
    @NotNull(message = "员工薪资不能为空", groups = D.class)
    @Min(value = 1000, message = "员工薪资最少为1000美金", groups = D.class)
    private Long salary;
    private Double bonusRate;

}
