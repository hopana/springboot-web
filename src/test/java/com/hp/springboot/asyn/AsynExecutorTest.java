package com.hp.springboot.asyn;

import com.hp.springboot.utils.AsynExecutor;
import java.util.concurrent.CountDownLatch;
import org.junit.Test;

/**
 * 测试 AsynExecutor
 *
 * @author hupan
 * @date 2021-09-28 21:56
 */
public class AsynExecutorTest {

    @Test
    public void testExecutor() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(100);

        for (int i = 0; i < 100; i++) {
            AsynExecutor.execute(new TaskThread(latch));
        }

        latch.await();
    }

}
