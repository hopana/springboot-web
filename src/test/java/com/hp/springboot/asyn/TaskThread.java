package com.hp.springboot.asyn;

import java.util.concurrent.CountDownLatch;
import lombok.SneakyThrows;

/**
 * 模拟任务线程
 *
 * @author hupan
 * @date 2021-09-28 21:59
 */
public class TaskThread implements Runnable {

    private final CountDownLatch countDownLatch;

    public TaskThread(CountDownLatch latch) {
        this.countDownLatch = latch;
    }

    @SneakyThrows
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            Thread.sleep(500);
            System.out.println(Thread.currentThread().getName() + ": " + i);
        }

        countDownLatch.countDown();
    }

}
