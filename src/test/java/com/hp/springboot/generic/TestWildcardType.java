package com.hp.springboot.generic;


import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * WildcardType represents a wildcard type expression, such as ?, ? extends Number, or ? super Integer.
 * 通配符表达式，或泛型表达式，它虽然是Type的一个子接口，但并不是Java类型中的一种，表示的仅仅是类似 ? extends T、? super K这样的通配符表达式。 ？
 * --通配符表达式，表示通配符泛型，但是WildcardType并不属于Java-Type中的一钟；
 * 例如：List< ? extends Number> 和 List< ? super Integer>；
 * 1、Type[] getUpperBounds();  //获得泛型表达式上界（上限） 获取泛型变量的上边界（extends）
 * 2、Type[] getLowerBounds(); //获得泛型表达式下界（下限） 获取泛型变量的下边界（super）
 *
 * @author: wangji
 * @date: 2018/06/25 19:47
 */
@Slf4j
public class TestWildcardType {

    /**
     * 1、 a: 获取ParameterizedType:? extends java.lang.Number 2、上界：class java.lang.Number
     */
    private List<? extends Number> a;

    /**
     * b: 获取ParameterizedType:? super java.lang.String 上届：class java.lang.Object 下届：class java.lang.String
     */
    private List<? super String> b;

    /**
     * c: 获取ParameterizedType:class java.lang.String
     */
    private List<String> c;

    /**
     * aClass: 获取ParameterizedType:? 上届：class java.lang.Object
     */
    private Class<?> aClass;

    private String wangji;

    /**
     * 多种数据进行混合
     */
    @Test
    public void testWildcardType() {
        try {
            Field[] fields = TestWildcardType.class.getDeclaredFields();
            for (Field field : fields) {
                log.info("begin ******当前field:" + field.getName() + " *************************");
                if (field.getGenericType() instanceof ParameterizedType) {
                    ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
                    for (Type type : parameterizedType.getActualTypeArguments()) {
                        log.info(field.getName() + ": 获取ParameterizedType:" + type);
                        if (type instanceof WildcardType) {
                            printWildcardType((WildcardType) type);
                        }
                    }
                } else if (field.getGenericType() instanceof GenericArrayType) {
                    GenericArrayType genericArrayType = (GenericArrayType) field.getGenericType();
                    log.info("GenericArrayType type :" + genericArrayType);
                    Type genericComponentType = genericArrayType.getGenericComponentType();
                    if (genericComponentType instanceof WildcardType) {
                        printWildcardType((WildcardType) genericComponentType);
                    }
                } else if (field.getGenericType() instanceof TypeVariable) {
                    TypeVariable typeVariable = (TypeVariable) field.getGenericType();
                    log.info("typeVariable:" + typeVariable);

                } else {
                    log.info("type :" + field.getGenericType());
                    if (field.getGenericType() instanceof WildcardType) {
                        printWildcardType((WildcardType) field.getGenericType());
                    }
                }
                log.info("end ******当前field:" + field.getName() + " *************************");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void printWildcardType(WildcardType wildcardType) {
        for (Type type : wildcardType.getUpperBounds()) {
            log.info("上界：" + type);
        }
        for (Type type : wildcardType.getLowerBounds()) {
            log.info("下界：" + type);
        }
    }

}