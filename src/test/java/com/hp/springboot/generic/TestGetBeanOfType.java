package com.hp.springboot.generic;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.TypeVariable;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.lang.NonNull;

/**
 * beanFactory.getBeansOfType 测试
 *
 * @author hupan
 * @date 2021-12-26 19:53
 */
@Slf4j
//@SpringBootTest
//@RunWith(SpringRunner.class)
public class TestGetBeanOfType<T extends Parent<?>> implements BeanFactoryAware {

    private ConfigurableListableBeanFactory beanFactory;

    @Test
    public void test() {
        TypeVariable<? extends Class<? extends TestGetBeanOfType>>[] typeParameters = this.getClass().getTypeParameters();
        ParameterizedType type = (ParameterizedType) typeParameters[0];
        System.out.println(type);

        // beanFactory.getBeansOfType()
    }

    @Override
    public void setBeanFactory(@NonNull BeanFactory beanFactory) throws BeansException {
        if (beanFactory instanceof DefaultListableBeanFactory) {
            this.beanFactory = (ConfigurableListableBeanFactory) beanFactory;
        }
    }
}
