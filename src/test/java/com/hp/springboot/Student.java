package com.hp.springboot;

/**
 * TODO 注释
 *
 * @author hupan
 * @date 2021-12-20 20:19
 */
public abstract class Student {

    abstract String getName();
    abstract Integer getAge();

    public void doHandle() {
        if ("Mike".equals(getName())) {
            System.out.println(getAge());
        }
    }

}
