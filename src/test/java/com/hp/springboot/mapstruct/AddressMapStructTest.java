package com.hp.springboot.mapstruct;

import com.hp.springboot.pojo.dto.DeliveryAddressDto;
import com.hp.springboot.pojo.dto.Person;
import com.hp.springboot.pojo.mapper.AddressMapper;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

/**
 * @author hupan
 * @date 2021-10-08
 */
public class AddressMapStructTest {

	AddressMapper MAPPER = Mappers.getMapper(AddressMapper.class);

	@Test
	public void testDirectlyReferringToASourceParameter() {
		Person p = new Person();
		p.setName("hupan");
		p.setAge(30);
		p.setDescription("测试直接映射字段");
		DeliveryAddressDto deliveryAddressDto = AddressMapper.INSTANCE.personAndAddressToDeliveryAddressDto(p, 10086);

		System.out.println(deliveryAddressDto.toString());
	}

	@Test
	public void testApplyExpressinToTargetField() {
		Person p = new Person();
		p.setName("hupan");
		p.setAge(30);
		p.setDescription("测试直接映射字段");
		DeliveryAddressDto deliveryAddressDto = AddressMapper.INSTANCE.personToDeliveryAddressDto(p);

		System.out.println(deliveryAddressDto.toString());
	}

	@Test
	public void testApplyConstantToTargetField() {
		Person p = new Person();
		p.setName("hupan");
		p.setAge(30);
		p.setDescription("测试直接映射字段");
		DeliveryAddressDto deliveryAddressDto = AddressMapper.INSTANCE.personToDeliveryAddressDtoUseConstant(p);

		System.out.println(deliveryAddressDto.toString());
	}

	@Test
	public void testOuterInstant() {
		Person p = new Person();
		p.setName("hupan");
		p.setAge(100);
		p.setDescription("测试外部INSTANT");
		DeliveryAddressDto deliveryAddressDto = MAPPER.personToDeliveryAddress(p);

		System.out.println(deliveryAddressDto.toString());
	}

}
