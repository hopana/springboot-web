package com.hp.springboot.mapstruct;

import com.hp.springboot.enums.UserTypeEnum;
import com.hp.springboot.pojo.dto.AnimalDto;
import com.hp.springboot.pojo.mapper.AnimalMapper;
import com.hp.springboot.pojo.model.Animal;
import com.hp.springboot.pojo.model.User;
import org.junit.Test;


/**
 * MapStruct测试
 *
 * @author hupan
 * @date 2021-09-24
 */
public class AnimalMapStructTest {

	@Test
	public void testMapWithCustomMethod() {
		Animal animal = new Animal();
		animal.setName("duck");
		animal.setAge(2);
		animal.setDesc("if it quacks like duck, walks like a duck its probably a duck.");

		User user = new User();
		user.setName("hupan");
		user.setAge(30);
		user.setUserType(UserTypeEnum.Java);
		animal.setOwner(user);

		AnimalDto animalDto = AnimalMapper.INSTANCE.animalToAnimalDto(animal);
		System.out.println(animalDto);
	}

}

