package com.hp.springboot.mapstruct;

import com.hp.springboot.enums.CarType;
import com.hp.springboot.pojo.dto.CarDto;
import com.hp.springboot.pojo.mapper.CarMapper;
import com.hp.springboot.pojo.model.Car;
import com.hp.springboot.pojo.model.User;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * MapStruct测试
 *
 * @author hupan
 * @date 2021-09-24
 */
public class CarMapStructTest {

	@Test
	public void testMap() {
		Car car = new Car("Morris", 5, CarType.SEDAN);
		CarDto carDto = CarMapper.INSTANCE.carToCarDto(car);

		assertThat(carDto).isNotNull();
		assertThat(carDto.getMake()).isEqualTo("Morris");
		assertThat(carDto.getSeatCount()).isEqualTo(5);
		assertThat(carDto.getType()).isEqualTo("SEDAN");
	}

	@Test
	public void testMultiMap() {
		Car car = new Car("Morris", 5, CarType.SEDAN);
		User user = new User();
		user.setName("hupan");
		user.setAge(30);

		CarDto carDto = CarMapper.INSTANCE.carToCarDto(car, user);

		assertThat(carDto).isNotNull();
		assertThat(carDto.getMake()).isEqualTo("Morris");
		assertThat(carDto.getSeatCount()).isEqualTo(5);
		assertThat(carDto.getType()).isEqualTo("SEDAN");
		assertThat(carDto.getOwner()).isEqualTo("hupan");
	}

	@Test
	public void testUpdateTarget() {
		Car car = new Car("Morris", 5, CarType.SEDAN);
		User user = new User();
		user.setName("hupan");
		user.setAge(30);

		CarDto carDto = new CarDto();
		carDto.setOwner("mike");
		carDto.setMake("bmw");
		carDto.setType("SEDAN");

		CarMapper.INSTANCE.updateTarget(car, user, carDto);

		assertThat(carDto).isNotNull();
		assertThat(carDto.getMake()).isEqualTo("Morris");
		assertThat(carDto.getSeatCount()).isEqualTo(5);
		assertThat(carDto.getType()).isEqualTo("SEDAN");
		assertThat(carDto.getOwner()).isEqualTo("hupan");
	}

	@Test
	public void testUpdateTargetAndReturn() {
		Car car = new Car("Morris", 5, CarType.SEDAN);
		User user = new User();
		user.setName("hupan");
		user.setAge(30);

		CarDto carDto = new CarDto();
		carDto.setOwner("mike");
		carDto.setMake("bmw");
		carDto.setType("SEDAN");

		CarDto returnObject = CarMapper.INSTANCE.updateTargetAndReturn(car, user, carDto);
		System.out.println(returnObject.toString());
	}

	@Test
	public void testMapWithDefaultValueAndConstant() {
		Car car = new Car();
		car.setMake("Morris");
		car.setType(CarType.SEDAN);

		User user = new User();
		user.setAge(30);

		CarDto carDto = new CarDto();
		carDto.setMake("bmw");
		carDto.setType("SEDAN");

		CarMapper.INSTANCE.carToCarDto(car, user, carDto);

		assertThat(carDto).isNotNull();
		assertThat(carDto.getSeatCount()).isEqualTo(5);
		assertThat(carDto.getOwner()).isEqualTo("hupan");
	}

	@Test
	public void testMapWithExpression() {
		Car car = new Car();
		car.setMake("Morris");
		car.setType(CarType.SEDAN);

		CarDto carDto = CarMapper.INSTANCE.carToCarDtoWithExpression(car);

		assertThat(carDto).isNotNull();
		assertThat(carDto.getModifyTime()).isNotNull();

		System.out.println(carDto.getModifyTime());
	}

}

