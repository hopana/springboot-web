package com.hp.springboot.mapstruct;

import com.hp.springboot.enums.UserTypeEnum;
import com.hp.springboot.pojo.dto.UserDto1;
import com.hp.springboot.pojo.dto.UserDto2;
import com.hp.springboot.pojo.mapper.UserMapper;
import com.hp.springboot.pojo.model.User;
import org.junit.Test;

import java.util.Date;


/**
 * MapStruct测试
 *
 * @author hupan
 * @date 2021-09-24
 */
public class UserMapStructTest {

	@Test
	public void testMapWithDateConvert() {
		User user = new User();
		user.setName("hupan");
		user.setAge(30);
		user.setAddress("深圳");
		user.setCreateTime(new Date());
		user.setUpdateTime(new Date());

		UserDto1 userDto = UserMapper.INSTANCE.toUserDto1(user);
		System.out.println(userDto);
	}

	@Test
	public void testMapWithEnum1() {
		User user = new User();
		user.setName("hupan");
		user.setAge(30);
		user.setAddress("深圳");
		user.setCreateTime(new Date());
		user.setUpdateTime(new Date());
		user.setUserType(UserTypeEnum.DB);

		UserDto2 userDto = UserMapper.INSTANCE.toUserDto2(user);
		System.out.println(userDto);
	}

	@Test
	public void testMapWithEnum2() {
		User user = new User();
		user.setName("hupan");
		user.setAge(30);
		user.setAddress("深圳");
		user.setCreateTime(new Date());
		user.setUpdateTime(new Date());
		user.setUserType(UserTypeEnum.DB);

		UserDto2 userDto = UserMapper.INSTANCE.toUserDto3(user);
		System.out.println(userDto);
	}


}

