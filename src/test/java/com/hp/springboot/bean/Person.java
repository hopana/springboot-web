package com.hp.springboot.bean;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 普通测试类
 *
 * @author hupan
 * @date 2021-09-08 23:27
 */
@Getter
@Setter
@ToString
public class Person {

    private String name;
    private int age;
    private Date birthdate;
    private int gender;
    private String addr;

}
