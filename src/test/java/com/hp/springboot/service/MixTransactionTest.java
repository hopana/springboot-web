package com.hp.springboot.service;

import com.hp.springboot.base.BaseServiceTest;
import javax.annotation.Resource;
import org.junit.Test;

public class MixTransactionTest extends BaseServiceTest {

    @Resource
    private MixTransactionalService mixTransactionalService;

    @Test
    public void testQueryAfterInsert1() {
        mixTransactionalService.test1();
    }

    @Test
    public void testQueryAfterInsert2() {
        mixTransactionalService.test2();
    }

    @Test
    public void testQueryAfterInsert3() {
        mixTransactionalService.test3();
    }

    @Test
    public void testQueryAfterInsert4() {
        mixTransactionalService.test4();
    }

}
