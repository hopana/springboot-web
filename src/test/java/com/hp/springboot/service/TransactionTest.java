package com.hp.springboot.service;

import com.hp.springboot.base.BaseServiceTest;
import com.hp.springboot.dao.SmsDao;
import com.hp.springboot.dao.UserDao;
import com.hp.springboot.pojo.model.Sms;
import com.hp.springboot.pojo.model.User;
import java.util.Date;
import javax.annotation.Resource;
import org.junit.Test;

public class TransactionTest extends BaseServiceTest {

    @Resource
    private UserDao userDao;
    @Resource
    private SmsDao smsDao;
    @Resource
    private UserService userService;
    @Resource
    private TestService testService;

    @Test
    public void testQueryAfterInsert() {
        User user = User.builder()
                        .name("hupan")
                        .age(30)
                        .idCard("422130199009013539")
                        .gender(1)
                        .mobile("18928491911")
                        .address("珠海")
                        .createTime(new Date())
                        .updateTime(new Date())
                        .build();

        Sms sms = Sms.builder().content("测试事务中先插入后查询").createTime(new Date()).updateTime(new Date()).build();
        userDao.saveUser(user);
        User dbUser = userDao.getById(user.getId());
        sms.setMobile(dbUser.getMobile());

        smsDao.saveSms(sms);
    }

    @Test
    public void testQueryAfterInsertInTransaction() {
        userService.SaveUserThenSaveSms();
    }

    @Test
    public void testTransactional1() {
        testService.testTransactional1();
    }

    @Test
    public void testTransactional2() {
        testService.testTransactional2();
    }

}
