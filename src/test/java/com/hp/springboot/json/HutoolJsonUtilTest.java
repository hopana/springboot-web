package com.hp.springboot.json;

import cn.hutool.json.JSONConfig;
import cn.hutool.json.JSONUtil;
import com.hp.springboot.bean.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试Hutool包的JsonUtil
 *
 * @author hupan
 * @date 2021-09-08 23:25
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class HutoolJsonUtilTest {

    @Test
    public void testToJsonStr() {
        Person person = new Person();
        person.setName("hupan");
        person.setAge(32);
        person.setGender(1);
        person.setBirthdate(null);
        person.setAddr("深圳西乡");


        String jsonNoNullValue = JSONUtil.toJsonStr(person);
        String jsonWithNullValue = JSONUtil.toJsonStr(JSONUtil.parse(person, JSONConfig.create().setIgnoreNullValue(false)));
        System.out.println(jsonNoNullValue);
        System.out.println(jsonWithNullValue);

        Person bean = JSONUtil.toBean(jsonWithNullValue, Person.class);
        System.out.println(bean.toString());
    }

}
