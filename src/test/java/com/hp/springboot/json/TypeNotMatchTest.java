package com.hp.springboot.json;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.hp.springboot.pojo.model.Employee;
import org.junit.Test;

/**
 * JSON字符串转对象
 *
 * @author hupan
 * @date 2021-12-16
 *
 * 结论：不管是Int转字符串还是字符串转Int都能正确识别；
 * 但是，当字符串超出Int范围时，GSON、FastJSON报错，Hutool不报错，转换后属性为null
 */
public class TypeNotMatchTest {

    /**
     * Gson：JSON字符串转对象，no、age字段类型不匹配
     * 结论： 不报错，字符串类型的数字能正确识别并转换成真实类型
     */
    @Test
    public void testString2IntWithGson() {
        String emploryJson = "{\n"
            + "    \"no\": \"78\",\n"
            + "    \"name\": \"大卫\",\n"
            + "    \"age\": \"59\",\n"
            + "    \"job\": \"保安\",\n"
            + "    \"adress\": \"深圳市南山区\",\n"
            + "    \"hireDate\": \"2014-02-28 18:03:17\",\n"
            + "    \"salary\": 12000,\n"
            + "    \"bonusRate\": 1.387,\n"
            + "    \"manager\": \"老李\"\n"
            + "}";

        Gson gson = new Gson();
        Employee employee = gson.fromJson(emploryJson, Employee.class);
        System.out.println(employee);
    }

    /**
     * Gson：JSON字符串转对象，no、age字段类型不匹配
     * 结论： 不报错，字符串类型的数字能正确识别并转换成真实类型
     */
    @Test
    public void testString2IntWithFastJson() {
        String emploryJson = "{\n"
            + "    \"no\": \"78\",\n"
            + "    \"name\": \"大卫\",\n"
            + "    \"age\": \"59\",\n"
            + "    \"job\": \"保安\",\n"
            + "    \"adress\": \"深圳市南山区\",\n"
            + "    \"hireDate\": \"2014-02-28 18:03:17\",\n"
            + "    \"salary\": 12000,\n"
            + "    \"bonusRate\": 1.387,\n"
            + "    \"manager\": \"老李\"\n"
            + "}";

        Employee employee = JSON.parseObject(emploryJson, Employee.class);
        System.out.println(employee);
    }

    /**
     * Gson：JSON字符串转对象，no、age字段类型不匹配
     * 结论： 不报错，字符串类型的数字能正确识别并转换成真实类型
     */
    @Test
    public void testString2IntWithHutool() {
        String emploryJson = "{\n"
            + "    \"no\": \"78\",\n"
            + "    \"name\": \"大卫\",\n"
            + "    \"age\": \"59\",\n"
            + "    \"job\": \"保安\",\n"
            + "    \"adress\": \"深圳市南山区\",\n"
            + "    \"hireDate\": \"2014-02-28 18:03:17\",\n"
            + "    \"salary\": 12000,\n"
            + "    \"bonusRate\": 1.387,\n"
            + "    \"manager\": \"老李\"\n"
            + "}";

        Employee employee = JSONUtil.toBean(emploryJson, Employee.class);
        System.out.println(employee);
    }


    /**
     * Gson：JSON字符串转对象，name字段类型不匹配
     * 结论： 不报错，数字能正确转成字符串
     */
    @Test
    public void testInt2StringWithGson() {
        String emploryJson = "{\n"
            + "  \"no\": 78,\n"
            + "  \"name\": 561,\n"
            + "  \"age\": 59,\n"
            + "  \"job\": \"保安\",\n"
            + "  \"adress\": \"深圳市南山区\"\n"
            + "}";

        Gson gson = new Gson();
        Employee employee = gson.fromJson(emploryJson, Employee.class);
        System.out.println(employee);
    }

    /**
     * Gson：JSON字符串转对象，name字段类型不匹配
     * 结论： 不报错，数字能正确转成字符串
     */
    @Test
    public void testInt2StringWithFastJson() {
        String emploryJson = "{\n"
            + "  \"no\": 78,\n"
            + "  \"name\": 561,\n"
            + "  \"age\": 59,\n"
            + "  \"job\": \"保安\",\n"
            + "  \"adress\": \"深圳市南山区\"\n"
            + "}";

        Employee employee = JSON.parseObject(emploryJson, Employee.class);
        System.out.println(employee);
    }

    /**
     * Gson：JSON字符串转对象，name字段类型不匹配
     * 结论： 不报错，数字能正确转成字符串
     */
    @Test
    public void testInt2StringWithHutool() {
        String emploryJson = "{\n"
            + "  \"no\": 78,\n"
            + "  \"name\": 561,\n"
            + "  \"age\": 59,\n"
            + "  \"job\": \"保安\",\n"
            + "  \"adress\": \"深圳市南山区\"\n"
            + "}";

        Employee employee = JSONUtil.toBean(emploryJson, Employee.class);
        System.out.println(employee);
    }

}
