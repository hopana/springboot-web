package com.hp.springboot.json;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.hp.springboot.pojo.model.Employee;
import org.junit.Test;

/**
 * JSON字符串转对象
 *
 * @author hupan
 * @date 2021-12-16
 */
public class JsonString2ObjectTest {

    /**
     * Gson：JSON字符串转对象
     * 结论：多了字段不报错
     */
    @Test
    public void testAttrRedundantWithGson() {
        String emploryJson = "{\n"
            + "    \"no\": 78,\n"
            + "    \"name\": \"大卫\",\n"
            + "    \"age\": 59,\n"
            + "    \"job\": \"保安\",\n"
            + "    \"adress\": \"深圳市南山区\",\n"
            + "    \"hireDate\": \"2014-02-28 18:03:17\",\n"
            + "    \"salary\": 12000,\n"
            + "    \"bonusRate\": 1.387,\n"
            + "    \"manager\": \"老李\"\n"
            + "}";

        Gson gson = new Gson();
        Employee employee = gson.fromJson(emploryJson, Employee.class);
        System.out.println(employee);
    }

    /**
     * FastJson：JSON字符串转对象
     * 结论：多了字段不报错
     */
    @Test
    public void testAttrRedundantWithFastJson() {
        String emploryJson = "{\n"
            + "    \"no\": 78,\n"
            + "    \"name\": \"大卫\",\n"
            + "    \"age\": 59,\n"
            + "    \"job\": \"保安\",\n"
            + "    \"adress\": \"深圳市南山区\",\n"
            + "    \"hireDate\": \"2014-02-28 18:03:17\",\n"
            + "    \"salary\": 12000,\n"
            + "    \"bonusRate\": 1.387,\n"
            + "    \"manager\": \"老李\"\n"
            + "}";

        Employee employee = JSON.parseObject(emploryJson, Employee.class);
        System.out.println(employee);
    }

    /**
     * Hutool工具类：JSON字符串转对象
     * 结论：多了字段不报错
     */
    @Test
    public void testAttrRedundantWithHutool() {
        String emploryJson = "{\n"
            + "    \"no\": 78,\n"
            + "    \"name\": \"大卫\",\n"
            + "    \"age\": 59,\n"
            + "    \"job\": \"保安\",\n"
            + "    \"adress\": \"深圳市南山区\",\n"
            + "    \"hireDate\": \"2014-02-28 18:03:17\",\n"
            + "    \"salary\": 12000,\n"
            + "    \"bonusRate\": 1.387,\n"
            + "    \"manager\": \"老李\"\n"
            + "}";

        Employee employee = JSONUtil.toBean(emploryJson, Employee.class);
        System.out.println(employee);
    }

    /**
     * Gson：JSON字符串转对象
     * 结论：缺少字段不报错，转对象后缺少的属性为null
     */
    @Test
    public void testAttrLackWithGson() {
        String emploryJson = "{\n"
            + "  \"no\": 78,\n"
            + "  \"name\": \"大卫\",\n"
            + "  \"age\": 59,\n"
            + "  \"job\": \"保安\",\n"
            + "  \"adress\": \"深圳市南山区\"\n"
            + "}";

        Gson gson = new Gson();
        Employee employee = gson.fromJson(emploryJson, Employee.class);
        System.out.println(employee);
    }

    /**
     * FastJson：JSON字符串转对象
     * 结论：缺少字段不报错，转对象后缺少的属性为null
     */
    @Test
    public void testAttrLackWithFastJson() {
        String emploryJson = "{\n"
            + "  \"no\": 78,\n"
            + "  \"name\": \"大卫\",\n"
            + "  \"age\": 59,\n"
            + "  \"job\": \"保安\",\n"
            + "  \"adress\": \"深圳市南山区\"\n"
            + "}";

        Employee employee = JSON.parseObject(emploryJson, Employee.class);
        System.out.println(employee);
    }

    /**
     * Hutool工具类：JSON字符串转对象
     * 结论：缺少字段不报错，转对象后缺少的属性为null
     */
    @Test
    public void testAttrLackWithHutool() {
        String emploryJson = "{\n"
            + "  \"no\": 78,\n"
            + "  \"name\": \"大卫\",\n"
            + "  \"age\": 59,\n"
            + "  \"job\": \"保安\",\n"
            + "  \"adress\": \"深圳市南山区\"\n"
            + "}";

        Employee employee = JSONUtil.toBean(emploryJson, Employee.class);
        System.out.println(employee);
    }

}
